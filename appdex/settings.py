# Scrapy settings for appdex project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'appdex'

SPIDER_MODULES = ['appdex.spiders']
NEWSPIDER_MODULE = 'appdex.spiders'

ITEM_PIPELINES = [
    'appdex.pipelines.AppdexPipeline',
    #'appdex.pipelines.JsonWriterPipeline'
    ]

REDIS_CHANNEL = 'appdex'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'appdex (+http://www.yourdomain.com)'

DOWNLOAD_DELAY = 1.5
