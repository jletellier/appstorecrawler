from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule

from appdex.items import AppdexItem

import re

class Windowsphonestore(CrawlSpider):
    name = 'windowsphonestore'
    allowed_domains = ['www.windowsphone.com']
    start_urls = ['http://www.windowsphone.com/en-us/store']

    rules = (
        Rule(SgmlLinkExtractor(allow=r'/en-us/store/app/', deny=r'/reviews'),
             callback='parse_app', follow=True),
        Rule(SgmlLinkExtractor(allow=r'/en-us/store/')),
    )

    def parse_app(self, response):
        hxs = HtmlXPathSelector(response)
        i = AppdexItem()
        i['storeId'] = 'MSFT'
        i['url'] = response.url
        # find app name
        name = hxs.select('//div[@id="application"]/h1/text()')
        i['name'] = name.extract()[0]
        # find app description
        desc = hxs.select('//div[@id="appDescription"]/div/pre/text()')
        i['desc'] = desc.extract()[0]



        # find price of app (0.00 of for free)
        price = hxs.select("//span[@itemprop='price']/text()").extract()[0]
        price = re.search("\d+,\d+", price)
        if price is not None:
            price = price.group(0)
            price = str(price).replace(',','.')
        else:
            price = "0.00"
        i['price'] = str(price)
        # find id of category
        categoryId = hxs.select("//div[@id='crumb']/a[1]/@href").extract()[0]
        i['categoryId'] = categoryId
        # find name of category
        categoryName = hxs.select("//div[@id='crumb']/a[1]/strong/text()").extract()[0]
        i['categoryName'] = categoryName
        # find app icon link
        appIconLink = hxs.select("//img[@class='appImage xlarge']/@src").extract()[0]
        i['appIconLink'] = appIconLink
        # find app id
        appIdRegex = re.compile("/([a-z0-9-][a-z0-9-]*)")
        urlComponents = appIdRegex.findall(response.url)
        appId = urlComponents[len(urlComponents)-1]
        #appId = re.search("/([a-z0-9-][a-z0-9-]*)[0]", response.url).group(1)
        i['appId'] = appId
        # find app screenshots
        appScreenshots = hxs.select("//div[@id='screenshots']/ul/li/a/@href").extract()
        i['appScreenshots'] = appScreenshots
        # find rating
        rating = hxs.select("//div[@id='rating']/meta[1]/@content").extract()[0]
        rating = rating.replace(',','.')
        rating = float(rating) if self.is_number(rating) else 0.00
        i['numberOfStars']=str(rating)

        # find producer id
        producerId = hxs.select("//div[@id='publisher']/a/@href").extract()
        if len(producerId) > 0:
            producerId = producerId[0]
            producerId = re.search("publisherId=(.*)&", producerId).group(1)
        else:
            producerId = "NONE"
        i['producerId'] = producerId
        if producerId == "NONE":
            producerName = hxs.select("//span[@itemprop='publisher']/text()").extract()[0]
        else:
            producerName = hxs.select("//div[@id='publisher']/a/text()").extract()[0]
        i['producerName'] = producerName

        return i

    @staticmethod
    def is_number(s):
        try:
            float(s)
            return True
        except ValueError:
            return False