import os

development = {
    "INDEX_STORE_DIR": "/tmp/testindex",
    "DB_ADDRESS": "sqlite:////tmp/appdex.db"
}

production = {
    "INDEX_STORE_DIR": "/home/appdex/app_index",
    "DB_ADDRESS": "postgresql://appstorecrawler:appstorecrawler@localhost:5432/appstorecrawler"
}

config = development
if os.getenv('APPDEX_ENV') == 'production':
    config = production
