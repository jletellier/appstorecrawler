from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, ForeignKey, Float, Text

import json

from appstorecrawler import settings

engine = create_engine(settings.config['DB_ADDRESS'], convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


class Store(Base):
    __tablename__ = 'stores'
    id = Column(Integer, primary_key=True)
    storeIdInScrapping = Column(String(4), unique=True)
    name = Column(String(255))

    def __init__(self, name=None, storeIdInScrapping=None):
        self.name = name
        self.storeIdInScrapping = storeIdInScrapping

    def __repr__(self):
        return '<Store %r>' % (self.name)


class Producer(Base):
    __tablename__ = 'producers'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    producerIdInStore = Column(String(255))
    storeId = Column(Integer, ForeignKey('stores.id'))

    store = relationship("Store", backref=backref('producers', order_by=id))

    def __init__(self, name=None, producerIdInStore=None, storeId=None):
        self.name = name
        self.producerIdInStore = producerIdInStore
        self.storeId = storeId

    def __repr__(self):
        return '<Producer %r>' % (self.name)


class Category(Base):
    __tablename__ = 'categories'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    categoryIdInStore = Column(String(255))
    storeId = Column(Integer, ForeignKey('stores.id'))

    store = relationship("Store", backref=backref('categories', order_by=id))

    def __init__(self, name=None, storeId=None, categoryIdInStore=None):
        self.name = name
        self.storeId = storeId
        self.categoryIdInStore = categoryIdInStore

    def __repr__(self):
        return '<category %r>' % (self.name)


class App(Base):
    __tablename__ = 'apps'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    description = Column(Text)
    url = Column(String(255))
    priceInCent = Column(Integer)
    appIconLink = Column(String(255))
    screenshots = Column(Text)
    numberOfStars = Column(Float)
    appIdInStore = Column(String(255), unique=True)
    storeId = Column(Integer, ForeignKey('stores.id'))
    producerId = Column(Integer, ForeignKey('producers.id'))
    categoryId = Column(Integer, ForeignKey('categories.id'))

    store = relationship("Store", backref=backref('apps', order_by=id))
    producer = relationship("Producer", backref=backref('apps', order_by=id))
    category = relationship("Category", backref=backref('apps', order_by=id))

    def __init__(self, name=None, description=None, url=None, priceInCent=None,
                 appIconLink=None, screenshots=None, numberOfStars=None,
                 appIdInStore=None, storeId=None, producerId=None,
                 categoryId=None):
        self.name = name
        self.description = description
        self.url = url
        self.priceInCent = priceInCent
        self.appIconLink = appIconLink
        if screenshots is not None:
            self.screenshots = json.dumps(screenshots)
        self.numberOfStars = numberOfStars
        self.appIdInStore = appIdInStore
        self.producerId = producerId
        self.storeId = storeId
        self.categoryId = categoryId

    def getScreenshots(self):
        return json.loads(self.screenshots)

    def __repr__(self):
        return '<App %r>' % (self.name)


class CalculatedSimilarity(Base):
    __tablename__ = 'calculatedSimilarity'
    firstAppId = Column(Integer, ForeignKey('apps.id'), primary_key=True)
    secondAppId = Column(Integer, ForeignKey('apps.id'), primary_key=True)
    similarityScore = Column(Float)

    firstApp = relationship("App", foreign_keys=[firstAppId],
                            backref=backref('calculatedSimilarities',
                                            order_by=similarityScore.desc()))
    secondApp = relationship("App", foreign_keys=[secondAppId])

    def __init__(self, firstAppId=None, secondAppId=None,
                 similarityScore=None):
        self.firstAppId = firstAppId
        self.secondAppId = secondAppId
        self.similarityScore = similarityScore

    def __repr__(self):
        return '<CalculatedSimilarity %r %r %r>' % (self.firstAppId,
                                                    self.secondAppId,
                                                    self.similarityScore)


def init_db():
    Base.metadata.create_all(bind=engine)
