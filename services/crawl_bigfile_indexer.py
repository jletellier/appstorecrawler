#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'pavel'

import os
import logging
import sys
import json

from whoosh.index import create_in, exists_in, open_dir

from appstorecrawler.services.AppSchema import AppSchema
from appstorecrawler.services.indexer import on_items
from appstorecrawler import settings

import time


class Timer:
    def __init__(self):
        self.start = time.clock()

    def now(self, desc=''):
        end = time.clock()
        int = end - self.start
        logging.info('{0} {1}'.format(int, desc))
        self.start = time.clock()


if __name__ == '__main__':
    store_dir = settings.config['INDEX_STORE_DIR']
    logging.basicConfig(level=logging.INFO)

    # setting up whoosh
    ix = None
    if not os.path.isdir(store_dir):
        os.mkdir(store_dir)
    if exists_in(store_dir, indexname="app_index"):
        ix = open_dir(store_dir, indexname="app_index")
    else:
        ix = create_in(store_dir, AppSchema, indexname="app_index")

    filename = sys.argv[1]

    with open(filename, 'r') as f:

        i = 0
        items = []

        timer = Timer()

        def save(items):
            timer.now('read')
            on_items(items, ix)
            timer.now('commit')
            items = []
            print i

        for line in f:
            line = line.strip()[:-1]

            items.append(json.loads(line))

            # on_item(item, ix)

            i += 1
            if i % 2048 == 0:
                save(items)

            pass

        save(items)
