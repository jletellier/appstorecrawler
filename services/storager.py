import sys
import json
from signal import signal, SIGTERM
import atexit
import logging

from redis import StrictRedis

from appstorecrawler.models import db_session
from appstorecrawler.models import Store, Category, Producer, App


def store_item(app, db):
    if db.query(App).filter(App.appIdInStore == app.get('appId')).count() > 0:
        return

    #get store for app
    storeOfApp = db.query(Store).filter(Store.storeIdInScrapping == app.get('storeId'))[0]

    #insert category if not present yet
    categoriesResult = db.query(Category).filter(Category.categoryIdInStore == app.get('categoryId')).filter(Category.storeId == storeOfApp.id)
    categoryOfApp = None
    if len(categoriesResult.all()) > 0:
        categoryOfApp = categoriesResult[0]
    else:
        categoryOfApp = Category(app.get('categoryName'), storeOfApp.id, app.get('categoryId'))
        db.add(categoryOfApp)
        db.commit()

    #insert producer if not present yet
    producersResult = db.query(Producer).filter(Producer.producerIdInStore == app.get('producerId'))
    producerOfApp = None
    if len(producersResult.all()) > 0:
        producerOfApp = producersResult[0]
    else:
        producerOfApp = Producer(app.get('producerName'), app.get('producerId'), storeOfApp.id)
        db.add(producerOfApp)
        db.commit()

    #create app
    priceInCent = int(float(app.get('price'))*100)
    screenshotsList = json.dumps(app.get('appScreenshots'))

    appToSave = App(app.get('name'), app.get('desc'), app.get('url'), priceInCent, app.get('appIconLink'), screenshotsList, app.get('numberOfStars'), app.get('appId'), storeOfApp.id, producerOfApp.id, categoryOfApp.id)
    db.add(appToSave)
    logging.info("Added document for '%s'" % app.get('name'))

if __name__ == '__main__':

    #insert app stores if not present
    if db_session.query(Store).count() < 3:
        playStore = Store("Play Store", "GOOG")
        appStore = Store("AppStore", "AAPL")
        windowsStore = Store("Windows Phone Store", "MSFT")
        db_session.add_all([playStore, appStore, windowsStore])
        db_session.commit()

    # closing writer on SIGTERM
    def handler():
        logging.info("Stopped listening.")
        db_session.close()
    signal(SIGTERM, lambda signum, stack_frame: sys.exit(1))
    atexit.register(handler)

    # setting up redis pubsub
    red = StrictRedis()
    pubsub = red.pubsub()
    pubsub.subscribe('appdex')

    # listen for items
    logging.info("Listen for items ...")
    for item in pubsub.listen():
        logging.debug("Added document for '%s'" % str(item))
        if item.get('type') == 'message':

            app = json.loads(item.get('data'))
            store_item(app, db_session)
            db_session.commit()

    db_session.close()
