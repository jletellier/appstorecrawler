from pprint import pprint
from flask import Flask, render_template, redirect, url_for, jsonify
from appstorecrawler.models import db_session as db
from appstorecrawler.models import App

from whoosh.index import exists_in, open_dir
from whoosh.qparser import QueryParser

from appstorecrawler import settings

app = Flask(__name__)

ix = None
storedir = settings.config['INDEX_STORE_DIR']
if exists_in(storedir, indexname="app_index"):
    ix = open_dir(storedir, indexname="app_index")


@app.route("/search", defaults={"text": None, "store": None}, methods=["GET"])
@app.route("/search/<text>", defaults={"store": None}, methods=["GET"])
@app.route("/search/<text>/store/<store>", methods=["GET"])
def search(text, store):
    if not text:
        return redirect(url_for("index"))

    if not ix:
        return redirect(url_for("index"))

    stores = ['goog', 'msft', 'aapl']

    result_ids = []
    with ix.searcher() as searcher:
        parser = QueryParser('name', ix.schema)
        if store:
            q = parser.parse(u'*%s* AND storeId:%s' % (text, stores[int(store) - 1]))
        else:
            q = parser.parse(u'*%s*' % text)
        pprint(q)
        results = searcher.search(q)
        result_ids = [hit['appId'] for hit in results]

    print(result_ids)

    if store:
        results = db.query(App).filter(App.appIdInStore.in_(result_ids)).filter_by(storeId=store).limit(30)
    else:
        results = db.query(App).filter(App.appIdInStore.in_(result_ids)).limit(30)

    jsonArray = []
    for element in results:
        jsonArray.append(app_to_json(element, None))

        similar_apps = element.calculatedSimilarities
        storeIds = [1, 2, 3]
        storeIds.remove(element.storeId)

        for similar_app in similar_apps:
            tmpApp = similar_app.secondApp
            if tmpApp.storeId in storeIds:
                storeIds.remove(tmpApp.storeId)
                jsonArray.append(app_to_json(tmpApp, similar_app))
            if not storeIds:
                break

    return jsonify(term=text, results=jsonArray)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/about")
def about():
    return render_template("about.html", page="about")


def app_to_json(app, similar_app):
    json = {
        "id": app.id,
        "name": app.name,
        "desc": app.description,
        "url": app.url,
        "appIconLink": app.appIconLink,
        "numberOfStars": app.numberOfStars,
        "producerName": app.producer.name,
        "storeId": app.storeId
    }
    if similar_app:
        json["similarity"] = similar_app.similarityScore
    return json


if __name__ == "__main__":
    app.run(debug=True)
