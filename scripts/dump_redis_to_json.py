'''
Created on Mar 25, 2013

@author: pavel
'''
from redis.client import StrictRedis

import sys

if __name__ == '__main__':
    

    
    red = StrictRedis()
    pubsub = red.pubsub()
    pubsub.subscribe('appdex')
    
    print('Listen for items...')
    
    with open(sys.argv[1], 'w+') as f:
        for item in pubsub.listen():
            print('doc %s' % str(item))
            
            if item.get('type') == 'message':
                f.write(item['data'])
                f.write(',\n')
                
                f.flush()    
        
    
    pass